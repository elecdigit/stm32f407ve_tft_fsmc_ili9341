/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "rtc.h"
#include "spi.h"
#include "gpio.h"
#include "fsmc.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "XPT2046_touch.h"
#include "ili9341.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint16_t x = 0, y = 0;
uint16_t xx = 0;
//uint16_t y1 = 0;

char str1[20];
char str2[20];
char str3[20];
char str4[20];
char str[20];
bool flag1;
uint16_t strw;
char czas_txt[20];
char data_txt[20];
uint8_t CompareSeconds;

RTC_TimeTypeDef RtcTime;
RTC_DateTypeDef RtcDate;
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_FSMC_Init();
  MX_RTC_Init();
  MX_SPI2_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */

	HAL_RTC_GetTime(&hrtc, &RtcTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &RtcDate, RTC_FORMAT_BIN);

	sprintf(data_txt, "%02d.%02d.20%02d", RtcDate.Date, RtcDate.Month, RtcDate.Year);
	sprintf(czas_txt, "%02d:%02d:%02d", RtcTime.Hours, RtcTime.Minutes, RtcTime.Seconds);


	lcdBacklightOn();
	lcdInit();
	lcdSetOrientation(LCD_ORIENTATION_LANDSCAPE);
	lcdFillRGB(COLOR_WHITE);

	// Napisz tekst na górze ekranu
	lcdSetTextFont(&Font20);
	lcdSetTextColor(COLOR_BLACK, COLOR_WHITE);
	lcdSetCursor(50, 5); // xy
	lcdPrintf(" www.stm32res.ru ");
	lcdSetCursor(5, 30); // xy
	lcdPrintf(data_txt);

	lcdSetCursor(200, 30); // xy
	lcdPrintf(czas_txt);

	lcdSetTextFont(&Font12);
	lcdSetCursor(5, 50); // xy
	lcdPrintf("http://maciej.kuzmiak.net");
	lcdSetTextFont(&Font20);

	// Rozpocznij rysowanie ramek
	lcdDrawRect(50, 70, 230, 30, COLOR_BLUE);

	lcdDrawRect(30, 120, 40, 30, COLOR_BLUE); // lcdDrawRect (x, y, w, h, color)
	lcdDrawRect(75, 120, 40, 30, COLOR_BLUE);
	lcdDrawRect(120, 120, 40, 30, COLOR_BLUE);
	lcdDrawRect(165, 120, 40, 30, COLOR_BLUE);
	lcdDrawRect(210, 120, 40, 30, COLOR_BLUE);
	lcdDrawRect(255, 120, 50, 30, COLOR_BLUE);

	lcdDrawRect(30, 155, 40, 30, COLOR_BLUE); // lcdDrawRect (x, y, w, h, color)
	lcdDrawRect(75, 155, 40, 30, COLOR_BLUE);
	lcdDrawRect(120, 155, 40, 30, COLOR_BLUE);
	lcdDrawRect(165, 155, 40, 30, COLOR_BLUE);
	lcdDrawRect(210, 155, 40, 30, COLOR_BLUE);
	lcdDrawRect(255, 155, 50, 30, COLOR_BLUE);
	// Zacznij wypełniać ramki liczbami
	lcdSetTextFont(&Font24);
	lcdSetTextColor(COLOR_BLACK, COLOR_WHITE);

	lcdSetCursor(45, 125); // x
	lcdPrintf("1");
	lcdSetCursor(90, 125); // x
	lcdPrintf("2");
	lcdSetCursor(135, 125); // x
	lcdPrintf("3");
	lcdSetCursor(180, 125); // x
	lcdPrintf("4");
	lcdSetCursor(225, 125); // x
	lcdPrintf("5");

	lcdSetCursor(45, 160); // x
	lcdPrintf("6");
	lcdSetCursor(90, 160); // x
	lcdPrintf("7");
	lcdSetCursor(135, 160); // x
	lcdPrintf("8");
	lcdSetCursor(180, 160); // x
	lcdPrintf("9");
	lcdSetCursor(225, 160); // x
	lcdPrintf("0");

	lcdSetCursor(263, 125); // x
	lcdPrintf("<-");

	lcdSetTextFont(&Font20);
	lcdSetCursor(265, 160); // x
	lcdPrintf("Ok");

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {

		HAL_RTC_GetTime(&hrtc, &RtcTime, RTC_FORMAT_BIN);
		HAL_RTC_GetDate(&hrtc, &RtcDate, RTC_FORMAT_BIN);

		if (RtcTime.Seconds != CompareSeconds) {

			sprintf(data_txt, "%02d.%02d.20%02d", RtcDate.Date,
					RtcDate.Month, RtcDate.Year);
			sprintf(czas_txt, "%02d:%02d:%02d", RtcTime.Hours,
					RtcTime.Minutes, RtcTime.Seconds);
			lcdSetCursor(5, 30); // xy
			lcdPrintf(data_txt);

			lcdSetCursor(200, 30); // xy
			lcdPrintf(czas_txt);


		}

		sprintf(str1, "x = %u", x);
		sprintf(str2, "y = %u", y);

		if (flag1 == 1) // Flaga, za pomocą której określamy, że przerwanie zostało wyzwolone
				{
			flag1 = 0;   // Zresetuj flagę ustawioną w procedurze przerwania
						 // sprawdź, który obszar został kliknięty na ekranie. Interesuje nas tylko
						 // wewnętrzna powierzchnia ramek

			if (x >= 31 && x <= 65)   // Współrzędne  x
					{
				if (y >= 119 && y <= 146)  // Współrzędne  y
						{
					xx = 1;   // Jeśli trafią, przekazujemy zmienną - numer 1
					sprintf(str3, "%u", xx);
					strcat(str, str3);      // Dodaj znak do wspólnej linii
					strw = strlen(str);
					sprintf(str4, "%u", strw);
				}
			}

			if (x >= 80 && x <= 109) {
				if (y >= 111 && y <= 148) {
					xx = 2;
					sprintf(str3, "%u", xx);
					strcat(str, str3);
					strw = strlen(str);
					sprintf(str4, "%u", strw);
				}
			}

			if (x >= 131 && x <= 158) {
				if (y >= 109 && y <= 151) {
					xx = 3;
					sprintf(str3, "%u", xx);
					strcat(str, str3);
					strw = strlen(str);
					sprintf(str4, "%u", strw);
				}
			}

			if (x >= 166 && x <= 203) {
				if (y >= 120 && y <= 151) {
					xx = 4;
					sprintf(str3, "%u", xx);
					strcat(str, str3);
					strw = strlen(str);
					sprintf(str4, "%u", strw);
				}
			}

			if (x >= 201 && x <= 250) {
				if (y >= 110 && y <= 151) {
					xx = 5;
					sprintf(str3, "%u", xx);
					strcat(str, str3);
					strw = strlen(str);
					sprintf(str4, "%u", strw);
				}
			}

			if (x >= 32 && x <= 80) {
				if (y >= 156 && y <= 181) {
					xx = 6;
					sprintf(str3, "%u", xx);
					strcat(str, str3);
					strw = strlen(str);
					sprintf(str4, "%u", strw);
				}
			}

			if (x >= 78 && x <= 110) {
				if (y >= 150 && y <= 181) {
					xx = 7;
					sprintf(str3, "%u", xx);
					strcat(str, str3);
					strw = strlen(str);
					sprintf(str4, "%u", strw);
				}
			}

			if (x >= 121 && x <= 165) {
				if (y >= 145 && y <= 181) {
					xx = 8;
					sprintf(str3, "%u", xx);
					strcat(str, str3);
					strw = strlen(str);
					sprintf(str4, "%u", strw);
				}
			}

			if (x >= 168 && x <= 203) {
				if (y >= 156 && y <= 181) {
					xx = 9;
					sprintf(str3, "%u", xx);
					strcat(str, str3);
					strw = strlen(str);
					sprintf(str4, "%u", strw);
				}
			}

			if (x >= 212 && x <= 247) {
				if (y >= 156 && y <= 181) {
					xx = 0;
					sprintf(str3, "%u", xx);
					strcat(str, str3);
					strw = strlen(str);
					sprintf(str4, "%u", strw);
				}
			}

			if (x >= 255 && x <= 280) {
				if (y >= 119 && y <= 150) {

				}
				//
			}

		}

		// Выводим то что врамках
		lcdSetTextFont(&Font20);
		lcdSetCursor(60, 78);   // x,y
		lcdPrintf(str);

		lcdSetTextFont(&Font20);
		lcdSetCursor(60, 200);   // x,y
		lcdPrintf(str1); //"x= "

		lcdSetTextFont(&Font20);
		lcdSetCursor(180, 200);   // x,y
		lcdPrintf(str2); //"y= "

		lcdSetTextFont(&Font20);
		lcdSetCursor(285, 75);   // x,y
		lcdPrintf(str4);

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* EXTI9_5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	if (GPIO_Pin == T_PEN_Pin) {
		if (XPT2046_TouchPressed()) {
			if (XPT2046_TouchGetCoordinates(&x, &y)) {
				flag1 = 1; // jeśli kliknąłeś na ekranie, ustaw flagę wyzwalającą kliknięcie
				lcdDrawPixel(x, y, COLOR_RED); // punkt zostanie narysowany po kliknięciu na ekranie
			}
		}
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
